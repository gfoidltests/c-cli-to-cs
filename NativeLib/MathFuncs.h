#pragma once

#include <stdexcept>

class MathFuncs
{
public:
    MathFuncs();
    ~MathFuncs();

    double Add(const double a, const double b);
    double Subtract(const double a, const double b);
    double Multiply(const double a, const double b);
    double Divide(const double a, const double b);
};