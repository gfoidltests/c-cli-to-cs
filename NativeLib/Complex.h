#pragma once

class Complex
{
public:
    double Re;
    double Im;

    Complex();
    Complex(const double re, const double im);

    void Print();

    Complex operator+(const Complex& z2);
};