#include <iostream>
#include "MathFuncs.h"

using namespace std;

MathFuncs::MathFuncs()
{
    cout << "Ctor MathFuncs" << endl;
}

MathFuncs::~MathFuncs()
{
    cout << "Dtor MathFuncs" << endl;
}

double MathFuncs::Add(const double a, const double b)
{
    return a + b;
}

double MathFuncs::Subtract(const double a, const double b)
{
    return a - b;
}

double MathFuncs::Multiply(const double a, const double b)
{
    return a * b;
}

double MathFuncs::Divide(const double a, const double b)
{
    if (b == 0) throw std::invalid_argument("b cannot be 0");

    return a / b;
}