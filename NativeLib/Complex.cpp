#include <iostream>
#include "Complex.h"

using namespace std;

Complex::Complex() : Re(0), Im(0) {}

Complex::Complex(const double re, const double im) : Re(re), Im(im) {}

void Complex::Print()
{
    cout << this->Re << " + i*" << this->Im << endl;
}

Complex Complex::operator+(const Complex& z2)
{
    return Complex(this->Re + z2.Re, this->Im + z2.Im);
}