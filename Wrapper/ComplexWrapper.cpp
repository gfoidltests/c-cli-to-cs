#include "ComplexWrapper.h"

namespace CppWrapper
{

    ComplexWrapper::ComplexWrapper()
    {
        _complex = new Complex();
    }

    ComplexWrapper::ComplexWrapper(const double re, const double im)
    {
        _complex = new Complex(re, im);
    }

    ComplexWrapper::ComplexWrapper(const Complex& z)
    {
        _complex = &(Complex)z;
    }

    ComplexWrapper::~ComplexWrapper()
    {
        delete _complex;
    }

    double ComplexWrapper::getRe()
    {
        return _complex->Re;
    }

    double ComplexWrapper::getIm()
    {
        return _complex->Im;
    }

    void ComplexWrapper::Print()
    {
        _complex->Print();
    }

    ComplexWrapper^ ComplexWrapper::operator+(const ComplexWrapper z2)
    {
        Complex sum = *_complex + *(z2._complex);

        return gcnew ComplexWrapper(sum);
    }
}