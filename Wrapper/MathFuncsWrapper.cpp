#include "MathFuncsWrapper.h"

namespace CppWrapper
{
    MathFuncsWrapper::MathFuncsWrapper()
    {
        _myCppClass = new MathFuncs();
    }

    MathFuncsWrapper::~MathFuncsWrapper()
    {
        delete _myCppClass;
    }

    double MathFuncsWrapper::Add(const double a, const double b)
    {
        return _myCppClass->Add(a, b);
    }

    double MathFuncsWrapper::Subtract(const double a, const double b)
    {
        return _myCppClass->Subtract(a, b);
    }

    double MathFuncsWrapper::Multiply(const double a, const double b)
    {
        return _myCppClass->Multiply(a, b);
    }

    double MathFuncsWrapper::Divide(const double a, const double b)
    {
        return _myCppClass->Divide(a, b);
    }
}