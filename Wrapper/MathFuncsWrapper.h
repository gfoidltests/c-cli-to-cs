#pragma once

#include "..\NativeLib\MathFuncs.h"

namespace CppWrapper
{
    public ref class MathFuncsWrapper
    {
    public:
        MathFuncsWrapper();
        ~MathFuncsWrapper();

        double Add(const double a, const double b);
        double Subtract(const double a, const double b);
        double Multiply(const double a, const double b);
        double Divide(const double a, const double b);

    private:
        MathFuncs* _myCppClass;
    };
}