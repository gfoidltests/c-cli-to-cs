#pragma once

#include "..\NativeLib\Complex.h"

namespace CppWrapper
{
    public ref class ComplexWrapper
    {
    public:
        ComplexWrapper();
        ComplexWrapper(const double re, const double im);
        ~ComplexWrapper();

        double getRe();
        double getIm();

        void Print();

        ComplexWrapper^ operator+(const ComplexWrapper z2);

    private:
        Complex* _complex;

        ComplexWrapper(const Complex& z);
    };
}