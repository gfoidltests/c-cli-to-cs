// not necessary in VS :-)
//#using "..\ManagedClass\bin\x86\Debug\ManagedClass.dll"

#include <msclr/auto_gcroot.h>
#include "FooWrapper.h"

using namespace ManagedClass;

namespace ManagedWrapper
{
    class FooWrapperPrivate
    {
    public:
        msclr::auto_gcroot<Foo^> foo;
    };

    FooWrapper::FooWrapper()
    {
        _private      = new FooWrapperPrivate();
        _private->foo = gcnew Foo();
    }

    FooWrapper::~FooWrapper()
    {
        delete _private;
    }

    void FooWrapper::Print()
    {
        _private->foo->Print();
    }
}