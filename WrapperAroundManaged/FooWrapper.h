#pragma once

namespace ManagedWrapper
{
    class FooWrapperPrivate;

    class FooWrapper
    {
    public:
        FooWrapper();
        ~FooWrapper();

        void Print();

    private:
        FooWrapperPrivate* _private;
    };
}