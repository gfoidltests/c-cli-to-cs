﻿using System;
using CppWrapper;

namespace Managed
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MathFuncsWrapper math = new MathFuncsWrapper())
            {
                double c = math.Add(0.1, 0.95);
                Console.WriteLine(c);

                //c = math.Divide(3, 0);
            }

            Console.WriteLine();

            using (ComplexWrapper z1 = new ComplexWrapper(3, 4))
            using (ComplexWrapper z2 = new ComplexWrapper(1, 2))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                z1.Print();
                z2.Print();
                //ComplexWrapper z3 = z1 + z2;
                //z3.Print();
                Console.ResetColor();
            }
        }
    }
}