﻿using System;

namespace ManagedClass
{
    public class Foo
    {
        private static int   _instanceCounter = 0;
        private readonly int _id              = _instanceCounter++;

        public void Print()
        {
            Console.WriteLine("Foo #{0}", _id);
        }
    }
}