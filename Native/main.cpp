#include <iostream>
#include "..\NativeLib\MathFuncs.h"
#include "..\NativeLib\Complex.h"
#include "..\WrapperAroundManaged\FooWrapper.h"

using namespace std;

void mathFuncs();
void complex();
void foo();

int main()
{
    mathFuncs();
    cout << endl;

    complex();
    cout << endl;

    foo();
    cout << endl;

    return 0;
}

void mathFuncs()
{
    MathFuncs* m = new MathFuncs();     // on Heap
    double c = m->Add(3.1, 2.9);
    cout << c << endl;
    delete m;

    MathFuncs m1;                       // on Stack
    c = m1.Add(0.9, 0.1);
    cout << c << endl;
}

void complex()
{
    Complex* z = new Complex(3, 4);
    z->Print();
    
    Complex z1(4, 5);
    z1.Print();

    Complex z2 = *z + z1;
    z2.Print();
}

void foo()
{
    using namespace ManagedWrapper;

    FooWrapper foo1;
    FooWrapper foo2;

    foo1.Print();
    foo2.Print();

    FooWrapper* foo3 = new FooWrapper();
    foo3->Print();
    delete foo3;

    foo3 = new FooWrapper();
    foo3->Print();
    delete foo3;
}